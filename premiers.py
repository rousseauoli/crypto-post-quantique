import random

def petitsPremiers(N):
    premiers = [2]
    for i in range (3,N):
        prem = 1
        for p in premiers:
          if i%p == 0:
            prem = 0
        if prem == 1:
          premiers.append(i)
    return premiers

premiers = petitsPremiers(100)

def TemoinRM(n, a):
  d = n-1
  s = 0
  while d%2 == 0:
    d = d // 2
    s += 1
  x = pow(a, d, n)
  if x == 1 or x == n-1:
    return False
  for i in range(s-1):
    x = pow(x, 2, n)
    if x == n-1:
      return False
  return True

def testerPremierRM(n, N):
  for p in premiers:
    if n%p == 0:
      return False
  for i in range(N):
    a = random.randrange(2, n-2)
    if TemoinRM(n, a):
      return False
  return True

def genererPremierRM(nbits, N):
  while (True):
    n = random.randrange(2**(nbits-1), 2**nbits)
    if n%2 ==0:
      n = n+1
    if (testerPremierRM(n, N)):
      return n

def genererGrandPremier(nbits):
  return genererPremierRM(nbits, 5)